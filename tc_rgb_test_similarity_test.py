import cv2
import random
import math
from colormath.color_diff import delta_e_cie2000
from colormath.color_objects import sRGBColor, XYZColor, LabColor
from colormath.color_conversions import convert_color
from colormath import color_conversions
import tkinter as tk
import json
from model_4 import predict
import base64
from PIL import Image
from io import BytesIO
import numpy as np
from scipy.spatial import distance
from PIL import Image, ImageDraw, ImageFilter
import colorspacious as cs
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from pyzbar.pyzbar import decode
import openpyxl
import time
import psutil

"""
三原色RGB试剂条还原
"""


def save_detection(img, filename, path):
    """ 根据模型发现试剂
       :param im_data: (numpy.ndarray) 输入图片
       :return: (numpy.ndarray) 发现区域后，裁剪置信度大于0.9的区域图片
       """
    # 推理图像
    result = predict.detection(img)

    boxes = result.boxes
    scores = result.scores
    label_ids = result.label_ids
    detection_results = [
        DetectionResult(*box, score, label_id)
        for box, score, label_id in zip(boxes, scores, label_ids)
    ]

    highest_scores = {}

    # Iterate through all results
    for result in detection_results:
        label_id = result.label_id
        score = result.score

        # Check if this label_id has been seen before or if the current score is higher
        if label_id not in highest_scores or score > highest_scores[label_id]:
            highest_scores[label_id] = score

    # Filter the detection_results to keep only the highest-scored entry for each label_id
    filtered_results = [
        result
        for result in detection_results
        if result.score == highest_scores[result.label_id]
    ]

    # Print the final results
    for result in filtered_results:
        print(
            f"File: {filename}, Label ID: {result.label_id}, "
            f"Box: ({result.xmin}, {result.ymin}, {result.xmax}, {result.ymax}), "
            f"Score: {result.score}"
        )

    # 在图像上绘制边界框并显示
    img_with_boxes = img.copy()
    for result in filtered_results:
        cv2.rectangle(img_with_boxes, (int(result.xmin), int(result.ymin)),
                      (int(result.xmax), int(result.ymax)), (0, 255, 0), 2)

    # file_path = os.path.join(path, filename)0
    # cv2.imwrite(file_path, img_with_boxes)

    # vis_im = vision.vis_detection(img, detection_results, score_threshold=0.001)
    # file_path = os.path.join(path, filename)
    # cv2.imwrite(file_path, vis_im)


class DetectionResult:
    def __init__(self, xmin, ymin, xmax, ymax, score, label_id):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.score = score
        self.label_id = label_id

        # Initialize filtered attributes
        self.filtered_xmin = xmin
        self.filtered_ymin = ymin
        self.filtered_xmax = xmax
        self.filtered_ymax = ymax
        self.filtered_score = score
        self.filtered_label_id = label_id


def extract_color(img, box):
    # 提取颜色块区域的 RGB 值
    xmin, ymin, xmax, ymax = map(int, box)
    roi = img[ymin:ymax, xmin:xmax]
    avg_color_per_row = np.average(roi, axis=0)
    avg_color = np.average(avg_color_per_row, axis=0)
    return avg_color

def calculate_color_difference(color1, color2):
    # 使用 CIEDE2000 公式计算色差
    color1_rgb = sRGBColor(*color1, is_upscaled=True)
    color2_rgb = sRGBColor(*color2, is_upscaled=True)
    color1_lab = convert_color(color1_rgb, LabColor)
    color2_lab = convert_color(color2_rgb, LabColor)
    delta_e = delta_e_cie2000(color1_lab, color2_lab)
    return delta_e

def correct_color(img, red_diff, green_diff, blue_diff):
    # 根据色差对整幅图像进行颜色校正
    corrected_image = img.copy()
    for i in range(corrected_image.shape[0]):
        for j in range(corrected_image.shape[1]):
            r, g, b = corrected_image[i, j]
            corrected_r = np.clip(r - red_diff, 0, 255)
            corrected_g = np.clip(g - green_diff, 0, 255)
            corrected_b = np.clip(b - blue_diff, 0, 255)
            corrected_image[i, j] = [corrected_r, corrected_g, corrected_b]
    return corrected_image


def similarity(image):


    img = cv2.imread(image)
    height = img.shape[0]
    weight = img.shape[1]
    print("图像分辨率", weight, "*", height)
    cv2.imshow("Original Image", img)
    cv2.waitKey(1)  # 添加短暂的等待时间

    filtered_results = predict.detection(img, "aaa")
    R_box = list()
    G_box = list()
    B_box = list()
    T_box = list()
    C_box = list()
    for result in filtered_results:
        box = [result.xmin, result.ymin, result.xmax, result.ymax]
        ID = result.label_id
        if (ID == 0): R_box = box
        if (ID == 1): G_box = box
        if (ID == 2): B_box = box
        if (ID == 3): T_box = box
        if (ID == 4): C_box = box
    if len(T_box) == 0:
        return "0.01", "", "", ""
    if len(R_box) == 0 or len(G_box) == 0 or len(B_box) == 0:
        return "-1000", "", "", ""
    if len(C_box) == 0:
        return "-100", "", "", ""

    # 提取颜色块的 RGB 值
    red_rgb = extract_color(img, R_box)
    green_rgb = extract_color(img, G_box)
    blue_rgb = extract_color(img, B_box)

    print(f"Red RGB: {red_rgb}")
    print(f"Green RGB: {green_rgb}")
    print(f"Blue RGB: {blue_rgb}")

    # 定义标准颜色的 RGB 值
    standard_red_rgb = [255, 0, 0]
    standard_green_rgb = [0, 255, 0]
    standard_blue_rgb = [0, 0, 255]

    # 计算色差
    red_difference = calculate_color_difference(red_rgb, standard_red_rgb)
    green_difference = calculate_color_difference(green_rgb, standard_green_rgb)
    blue_difference = calculate_color_difference(blue_rgb, standard_blue_rgb)

    print(f"Red color difference: {red_difference}")
    print(f"Green color difference: {green_difference}")
    print(f"Blue color difference: {blue_difference}")

    # 进行颜色还原
    corrected_image = correct_color(img, red_difference, green_difference, blue_difference)



    cv2.imshow("Corrected Image", corrected_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # 保存图像以便查看
    corrected_image_path = 'corrected_image.jpg'
    cv2.imwrite(corrected_image_path, corrected_image)
    print(f"Corrected image saved to {corrected_image_path}")





if __name__ == '__main__':
    similarity("uploads/Redmi_tmp_ac622dfc518d36543532d27c33071999f04424f8d4b15d64.jpg")


