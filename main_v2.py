import json

from flask import Flask, request, jsonify
import tc_rgb_test_similarity as test
# import tc_rgb_test_similarity_v1 as test
import os
from datetime import datetime

app = Flask(__name__)


##tc_rgb_test_similarity
@app.route('/upload_file', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    headers_content = dict(request.headers)
    Brand = headers_content['Brand']
    print(headers_content)
    Phototime = headers_content['Phototime']
    flashStatus = headers_content['Flashstatus']
    parsed_time = datetime.strptime(Phototime, '%Y-%m-%d %H:%M:%S.%f')
    formatted_time = parsed_time.strftime('%Y%m%d%H%M%S_%f')[:-3]

    if file.filename == '':
        return jsonify({'error': 'No selected file'})
    print(file.filename)

    # 保存上传的文件至服务器
    fileName = 'uploads/' + Brand + "_" + flashStatus + "_" + formatted_time + "_" + file.filename
    file.save(fileName)

    # 相似值
    try:
        result = test.similarity(fileName)
        result = json.loads(result)
        similarity = result['result']
    except Exception as e:
        similarity = -2000
        print(e)

    # 返回值
    return_data = jsonify({
        'code': 200,
        'value': similarity,
    })

    with open("0.txt", 'a') as f:
        f.write(str(similarity) + "\n")  # Write the dictionary entry as a string to the file

    ##os.remove('uploads/' + file.filename)
    return return_data
    # return jsonify({'message': 'File uploaded successfully', 'filename': file.filename})


# http服务器
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8888, threaded=True)
