import fastdeploy as fd
import fastdeploy.vision as vision
import cv2
import os
import random
import numpy as np
import math
from pyzbar.pyzbar import decode







cap = cv2.VideoCapture(0)
while True:
    ret, frame = cap.read()
    # Convert the image to grayscale
    gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Use Pyzbar library to detect and decode barcodes
    barcodes = decode(gray_image)

    print(barcodes)

    # Loop through detected barcodes and print their data
    for barcode in barcodes:
        barcode_data = barcode.data.decode("utf-8")
        print("Barcode Data:", barcode_data)

        # Draw a rectangle around the barcode
        x, y, w, h = barcode.rect
        cv2.rectangle(gray_image, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cv2.imshow('Object Detection', gray_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()