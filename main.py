from flask import Flask, request
import json
import test_similarity

app = Flask(__name__)


@app.route('/calculate', methods=['GET'])
def get():
    # url参数校验
    url = request.args['url']
    if url == '':
        return '{"code":500,value":[],"url": ""}'

    # 相似值
    try:
        similarity = test_similarity.similarity(url)
    except Exception as e:
        similarity = -2000
        print(e)

    # 返回值
    print("返回结果: " + str(similarity))
    return_data = json.dumps({
        'code': 200,
        'value': similarity,
        'url': url
    })
    return return_data


# http服务器
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8887, threaded=True)
