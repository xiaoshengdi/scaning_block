import copy
import math
import urllib.request

import cv2
import numpy as np

from model import predict


# 计算数组平均值 ， 去掉一个最大值， 去掉一个最小值
def average(data_list):
    # 去除0的情况
    data_list_none_zero = []
    for i in range(len(data_list)):
        if data_list[i] != 0:
            data_list_none_zero.append(data_list[i])
    data_list = data_list_none_zero[2:-2]

    if len(data_list) == 0:
        return 0
    if len(data_list) > 2:
        data_list.remove(min(data_list))
        data_list.remove(max(data_list))
        average_data = float(sum(data_list)) / len(data_list)
        return average_data
    elif len(data_list) <= 2:
        average_data = float(sum(data_list)) / len(data_list)
        return average_data


# 计算一个图片的测试值
def test_average(image):
    height = image.shape[0]
    weight = image.shape[1]
    count = height * weight

    # 循环获取像素值
    pixel_values = []
    for row in range(height):  # 遍历高
        for col in range(weight):  # 遍历宽
            pixel_values.append(image[row, col])

    # # 如果当前数量小于c线的大小， 需要补齐
    # if count < c_count:
    #     for i in range(c_count - count):
    #         pixel_values.append(1)
    # # 如果当前数量大于c线的大小， 需要删除相应数量
    # if count > c_count:
    #     for i in range(count - c_count):
    #         pixel_values.pop()
    # 计算平均值
    result = average(pixel_values)
    return result


# 计算测试值
def calculate_test_value(new_images):
    t_average = test_average(new_images[1])
    c_average = test_average(new_images[2])
    # print("原T值：", t_average)
    # print("原C值：", c_average)
    # t_average = find_closest_multiple_of_5(t_average)
    # c_average = find_closest_multiple_of_5(c_average)
    print("T值：", t_average)
    # 判断是否为nan
    if math.isnan(t_average):
        return 0.02, '', '', ''
    print("C值：", c_average)
    # 判断是否为nan
    if math.isnan(c_average) or c_average == 0:
        return -1002, '', '', ''
    # 获取中间点的像素平均值
    median_average = test_average(new_images[0])
    # print("中间值：", median_average)
    # 计算平均值
    # result = (int(t_average) - int(median_average)) / (int(c_average) - int(median_average))
    # result = int(t_average - median_average) / int(c_average - median_average))
    result = (t_average - median_average) / (c_average - median_average)
    # result = t_average / c_average
    similarity_result = round(abs(result), 2)
    # print("T/C：", similarity_result)
    return similarity_result, t_average, c_average, median_average


def find_closest_multiple_of_5(num):
    remainder = num % 5  # 计算num对5取余数
    if remainder == 0:
        result = num  # 如果num已经能够被5整除，则结果为num
    else:
        result1 = num - remainder  # 计算比num小的最大能够被5整除的数
        result2 = result1 + 5  # 计算比num大的最小能够被5整除的数
        if abs(num - result1) < abs(num - result2):  # 比较两个结果的差值
            result = result1
        else:
            result = result2
    return result


# 计算灰度1  使用opencv默认方法
def calculate_gray1(img):
    return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


# 计算灰度2  使用伽马变换
def calculate_gray2(img):
    # 灰度化处理：此灰度化处理用于图像二值化
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # 伽马变换
    gamma = copy.deepcopy(gray)
    rows = img.shape[0]
    cols = img.shape[1]
    for i in range(rows):
        for j in range(cols):
            gamma[i][j] = 3 * pow(gamma[i][j], 0.8)
    return gamma


# 计算灰度3  使用对数变换
def calculate_gray3(img):
    # 灰度化处理：此灰度化处理用于图像二值化
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # 对数变换
    logc = copy.deepcopy(gray)
    rows = img.shape[0]
    cols = img.shape[1]
    for i in range(rows):
        for j in range(cols):
            logc[i][j] = 3 * math.log(1 + logc[i][j])
    return logc


# 图像旋转
# original_pic 原始图像
# point_one 坐标点1
# point_two 坐标点2
def image_revolve(original_pic, point_one, point_two):
    # cv2.imshow("original_pic", original_pic)
    # 计算向量差值
    dx = point_two[0] - point_one[0]
    dy = point_two[1] - point_one[1]
    # 计算弧度和角度（以弧度为单位）
    radians = math.atan2(dy, dx)
    degrees = math.degrees(radians) - 90
    # 原始图像的高度和宽度
    height, width = original_pic.shape[:2]
    # 定义旋转角度和缩放比例
    angle = degrees
    scale = 1.0
    # 计算旋转矩阵，并执行图像变换操作
    M = cv2.getRotationMatrix2D((width / 2, height / 2), angle, scale)
    rotated_img = cv2.warpAffine(original_pic, M, (width, height))
    # 显示原始图像和旋转后的图像
    # cv2.imshow("Original Image", rotated_img)
    return rotated_img


# 获取c线或t线的灰度区域
def get_line_area(original_pic, line_box, index, **args):
    # 推理图像裁剪
    less_width = int((line_box[2] - line_box[0]) / 4)
    # less_width = 0
    line_area = original_pic[int(line_box[1] + 2): int(line_box[3] - 2),
                int(line_box[0] + less_width):int(line_box[2] - less_width)]
    # cv2.imshow("binary_line_area_" + index, line_area)
    line_area_gray = calculate_gray1(line_area)
    # cv2.imshow("binary_line_area_gray_" + index, line_area_gray)

    # start_row_index不为空，则直接取值
    if not args.get('filter_row') is None:
        filter_rows = args.get('filter_row')
        return line_area_gray.take(filter_rows, axis=0), [0]

    # 平均值
    line_average = np.average(line_area_gray)
    # 循环获取像素值
    pixel_rows = []
    height = line_area_gray.shape[0]
    for row in range(height):  # 遍历高
        row_average = np.average(line_area_gray[row])
        if line_average > row_average:
            pixel_rows.append(row)

    # 行长度
    row_length = len(pixel_rows)
    # 缩小的高度
    less_height = int((pixel_rows[row_length - 1] - pixel_rows[0]) / 3)
    # 过滤后的数据
    line_rows_filter = pixel_rows[less_height:(row_length - less_height)] if row_length > less_height else pixel_rows
    return line_area_gray.take(line_rows_filter, axis=0), line_rows_filter


def lineC(original_pic, width, height, box, **args):
    # 计算目标框中心点坐标
    center_x = (box[0] + box[2]) / 2
    center_y = (box[1] + box[3]) / 2

    # center_x = (int(box[0]) + int(box[2])) / 2
    # center_y = (int(box[1]) + int(box[3])) / 2
    center = (center_x, center_y)
    # print("C目标框中心点坐标为：", center)

    left = int(center_x - width / 2)
    top = int(center_y - height / 2)
    right = int(center_x + width / 2)
    bottom = int(center_y + height / 2)
    c_box = [left, top, right, bottom]
    # print("c_box：" + str(c_box))
    image_c = original_pic[top: bottom,
              left:right]
    # cv2.imshow('C', image_c)
    # image_analysis_TC("C", image_c, None)
    line_area_gray = calculate_gray1(image_c)
    if not args.get('filter_row') is None:
        filter_rows = args.get('filter_row')
        return line_area_gray.take(filter_rows, axis=0), [0]
        # 平均值
    line_average = np.average(line_area_gray)
    # 循环获取像素值
    pixel_rows = []
    height = line_area_gray.shape[0]
    for row in range(height):  # 遍历高
        row_average = np.average(line_area_gray[row])
        if line_average > row_average:
            pixel_rows.append(row)

    # print("c灰度值:",line_area_gray.take(pixel_rows, axis=0))
    # print("c灰度值:", test_average(line_area_gray.take(pixel_rows, axis=0)))
    # 行长度
    row_length = len(pixel_rows)
    # 缩小的高度
    less_height = int((pixel_rows[row_length - 1] - pixel_rows[0]) / 3)
    # 过滤后的数据
    line_rows_filter = pixel_rows[less_height:(row_length - less_height)] if row_length > less_height else pixel_rows
    return line_area_gray.take(pixel_rows, axis=0), pixel_rows


def lineT(original_pic, width, height, box, **args):
    # 宽度
    # width = int((box[2] - box[0]) / 2)
    # # 高度
    # height = int((box[3] - box[1]) / 2)

    # 计算目标框中心点坐标
    center_x = (box[0] + box[2]) / 2
    center_y = (box[1] + box[3]) / 2
    center = (center_x, center_y)
    # print("T目标框中心点坐标为：", center)

    left = int(center_x - width / 2)
    top = int(center_y - height / 2)
    right = int(center_x + width / 2)
    bottom = int(center_y + height / 2)
    t_box = [left, top, right, bottom]
    # print("t_box：", t_box)
    image_t = original_pic[top: bottom,
              left:right]
    # cv2.imshow('T', image_t)
    # image_analysis_TC("T", image_t, None)
    line_area_gray = calculate_gray1(image_t)
    if not args.get('filter_row') is None:
        filter_rows = args.get('filter_row')
        return line_area_gray.take(filter_rows, axis=0), [0]

        # 平均值
    line_average = np.average(line_area_gray)
    # 循环获取像素值
    pixel_rows = []
    height = line_area_gray.shape[0]
    for row in range(height):  # 遍历高
        row_average = np.average(line_area_gray[row])
        if line_average > row_average:
            pixel_rows.append(row)
    # print("t灰度值:",line_area_gray.take(pixel_rows, axis=0))
    # print("t灰度值:", test_average(line_area_gray.take(pixel_rows, axis=0)))
    # 行长度
    row_length = len(pixel_rows)
    # 缩小的高度
    less_height = int((pixel_rows[row_length - 1] - pixel_rows[0]) / 3)
    # 过滤后的数据
    line_rows_filter = pixel_rows[less_height:(row_length - less_height)] if row_length > less_height else pixel_rows
    return line_area_gray.take(pixel_rows, axis=0), pixel_rows


def get_C_area(original_pic, line_box, index, **args):
    # 推理图像裁剪
    less_width = int((line_box[2] - line_box[0]) / 4)
    # less_width = 0
    line_area = original_pic[int(line_box[1] + 2): int(line_box[3] - 2),
                int(line_box[0] + less_width):int(line_box[2] - less_width)]
    # cv2.imshow("binary_line_area_" + index, line_area)
    line_area_gray = calculate_gray1(line_area)
    # cv2.imshow("binary_line_area_gray_" + index, line_area_gray)

    # start_row_index不为空，则直接取值
    if not args.get('filter_row') is None:
        filter_rows = args.get('filter_row')
        return line_area_gray.take(filter_rows, axis=0), [0]

    # 平均值
    line_average = np.average(line_area_gray)
    # 循环获取像素值
    pixel_rows = []
    height = line_area_gray.shape[0]
    for row in range(height):  # 遍历高
        row_average = np.average(line_area_gray[row])
        if line_average > row_average:
            pixel_rows.append(row)

    # 行长度
    row_length = len(pixel_rows)
    # 缩小的高度
    less_height = int((pixel_rows[row_length - 1] - pixel_rows[0]) / 3)
    # 过滤后的数据
    line_rows_filter = pixel_rows[less_height:(row_length - less_height)] if row_length > less_height else pixel_rows
    return line_area_gray.take(pixel_rows, axis=0), pixel_rows


def mein(c_box, t_box, width_c, height_c, original_pic):
    center_x_c = (c_box[0] + c_box[2]) / 2
    center_y_C = (c_box[1] + c_box[3]) / 2

    center_x_t = (t_box[0] + t_box[2]) / 2
    center_y_t = (t_box[1] + t_box[3]) / 2

    # center_x_c = int((int(c_box[0]) + int(c_box[2])) / 2)
    # center_y_C = int((int(c_box[1]) + int(c_box[3])) / 2)
    center_c = (center_x_c, center_y_C)
    # print("C目标框中心点坐标为：", center_c)

    # center_x_t = int(int(t_box[0]) + int(t_box[2])) / 2
    # center_y_t = int(int(t_box[1]) + int(t_box[3])) / 2
    center_t = (center_x_t, center_y_t)
    # print("T目标框中心点坐标为：", center_t)

    x = (center_x_c + center_x_t) / 2
    y = (center_y_C + center_y_t) / 2

    # x = (int(center_x_c) + int(center_x_t)) / 2
    # y = (int(center_y_C) + int(center_y_t)) / 2

    center_cen = (x, y)
    # print("中目标框中心点坐标为：", center_cen)

    height = height_c
    width = width_c
    left = int(x - width / 2)
    top = int(y - height / 2)
    right = int(x + width / 2)
    bottom = int(y + height / 2)
    cen_box = [left, top, right, bottom]
    # print("cen_box：", cen_box)

    Center = original_pic[top: bottom,
             left:right]
    # cv2.imshow('Center', Center)
    line_area_gray = calculate_gray1(Center)
    return line_area_gray;


# original_pic 原始图像
# cropped_images 推理后的c/t线
# 抠图， 返回两条线（c线和t线）x
# c线为第一位，t线为第二位
def image_cutout(original_pic, t_box,c_box):
    # 轮廓对应x，y， w，h 值
    # x = 0 y = 1 w = 2 h  = 3
    # DetectionResult，图像检测出来的目标框，每个框以4个float数值依次表示xmin, ymin, xmax, ymax， 即左上角和右下角坐标.
    # new_image = image[y + 2:y + h - 2, x + 2:x + w - 2]



    # 宽度
    width_c = int((c_box[2] - c_box[0]))
    # 高度
    heigth_c = int((c_box[3] - c_box[1]) / 2)

    cen = mein(c_box, t_box, width_c, heigth_c, original_pic)
    c, cd = lineC(original_pic, width_c, heigth_c, c_box)
    t, td = lineT(original_pic, width_c, heigth_c, t_box)

    # a = ((int(c_box[3]) - int(c_box[1])) + (int(t_box[3]) - int(t_box[1]))) * 0.5 / 4
    # 中间区域， C线右移, 偏移量：T与C线的中间值
    # int(c_box[2]) - int(c_box[0])代表 c线的宽度
    # median_index = int(xs[c_index] + (xs[t_index] - xs[c_index]) / 2)
    # median_image = original_pic[int(c_box[1] + a): int(c_box[3] - a),
    #               int(median_index + 2): int(median_index + c_box[2] - c_box[0] - 2)]
    # median_gray = calculate_gray1(median_image)
    # cv2.imshow("binary_median_area", median_gray)

    # c_gray, c_line_rows_filter = get_line_area(original_pic, c_box, str(1))
    # cv2.imshow("binary_c_area", c_gray)

    # t_gray, d_line_rows_filter = get_line_area(original_pic, t_box, str(2), filter_row=c_line_rows_filter)
    # t_gray, d_line_rows_filter = get_line_area(original_pic, t_box, str(2))
    # cv2.imshow("binary_t_area", t_gray)

    # 新裁剪的灰度图片，0：中间区域 1 灰度t 2 灰度c
    # tailor_images = [median_gray, t_gray, c_gray]
    tailor_images = [cen, t, c]
    return tailor_images


def image_analysis_TC(name, imageTC, imageUrl):
    if imageUrl is not None:
        res = urllib.request.urlopen(imageUrl)
        image = np.asarray(bytearray(res.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        # 读取图像并将其转换为 HSV 格式。
        hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        brightness = np.mean(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
    else:
        image = imageTC
        hsv_image = cv2.cvtColor(imageTC, cv2.COLOR_BGR2HSV)
        brightness = np.mean(cv2.cvtColor(imageTC, cv2.COLOR_BGR2GRAY))

    # 计算图像的平均亮度。
    print(name, ":计算图像的平均亮度", brightness)

    # 计算图像的平均对比度。
    mean_brightness = np.mean(hsv_image[:, :, 2])
    brightness_diff_sum = 0
    for pixel in hsv_image[:, :, 2].flat:
        brightness_diff_sum += abs(pixel - mean_brightness)
    contrast = brightness_diff_sum / (hsv_image.shape[0] * hsv_image.shape[1])
    print(name, ":计算图像的平均对比度", contrast)
    # 计算图像的锐化程度。
    blurred_image = cv2.GaussianBlur(image, (0, 0), 3)
    unsharp_image = cv2.addWeighted(image, 1.5, blurred_image, -0.5, 0)
    sharpeness = np.mean(cv2.cvtColor(unsharp_image, cv2.COLOR_BGR2GRAY))
    print(name, ":计算图像的锐化程度", sharpeness)

    # 计算图像的饱和度。
    saturation = np.mean(hsv_image[:, :, 1])
    print(name, ":计算图像的饱和度", saturation)

    # 计算图像的色调。
    hue = np.mean(hsv_image[:, :, 0])
    print(name, ":计算图像的色调", hue)

    # 计算图像的白平衡。
    white_patch = hsv_image[hsv_image.shape[0] // 2 - 10:hsv_image.shape[0] // 2 + 10,
                  hsv_image.shape[1] // 2 - 10:hsv_image.shape[1] // 2 + 10, :]
    mean_white = np.mean(white_patch, axis=(0, 1))
    white_balance = [255 / mean_white[0] * hsv_image[:, :, 0],
                     255 / mean_white[1] * hsv_image[:, :, 1],
                     255 / mean_white[2] * hsv_image[:, :, 2]]
    white_balanced_image = cv2.merge(white_balance).astype('uint8')
    # print("计算图像的白平衡",white_balanced_image)


# 计算结果
def similarity(image):
    rotated_img = cv2.imread(image)
    filtered_results = predict.detection(rotated_img)
    R_box = list()
    G_box = list()
    B_box = list()
    T_box = list()
    C_box = list()
    for result in filtered_results:
        box = [result.xmin, result.ymin, result.xmax, result.ymax]
        ID = result.label_id
        if (ID == 0): R_box = box
        if (ID == 1): G_box = box
        if (ID == 2): B_box = box
        if (ID == 3): T_box = box
        if (ID == 4): C_box = box
    if len(T_box) == 0:
        return "0.01"


    tailor_images = image_cutout(rotated_img, T_box,C_box)
    # 计算t/c值
    similarity_result, t_average, c_average, median_average = calculate_test_value(tailor_images)
    return similarity_result


# ts = similarity("https://qiniu.preova.com/IIA_37_1684301192363.jpg")
# ts = similarity("https://qiniu.preova.com/IIA_4608_1684383345604.jpg")
# ts = similarity("https://qiniu.preova.com/IIA_4608_1684409865335.png") #580
# ts = similarity("https://qiniu.preova.com/IIA_4608_1684410307704.png")
# ts = similarity("https://qiniu.preova.com/IIA_4644_1686205616154.jpg")
# cv2.waitKey(0)
