# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify
import os
import json
from datetime import datetime

app = Flask(__name__)
testItem = ''
concentration = ''
quality = ''


@app.route('/upload_file', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    if file.filename == '':
        return jsonify({'error': 'No selected file'})
    print(file.filename)

    headers_content = dict(request.headers)
    Devicebrand = headers_content['Devicebrand']
    Brand = headers_content['Brand']
    Phototime = headers_content['Phototime']
    parsed_time = datetime.strptime(Phototime, '%Y-%m-%d %H:%M:%S.%f')
    formatted_time = parsed_time.strftime('%Y%m%d%H%M%S_%f')[:-3]
    quality = request.form.get('quality', 'Default Quality Value')  # 默认值为 'Default Quality Value'
    testItem = request.form.get('testItem', 'Default TestItem Value')  # 默认值为 'Default Quality Value'
    concentration = request.form.get('concentration', 'Default Concentration Value')  # 默认值为 'Default Quality Value'
    testTime = request.form.get('testTime', 'Default Concentration Value')  # 默认值为 'Default Quality Value'
    # 保存上传的文件至服务器
    file.save(
        'uploads/' + Brand + "_" + testItem + "_" + concentration + "_" + quality + "_" + testTime + "_" + formatted_time + "_" + file.filename)

    # 相似值

    # 返回值
    return_data = jsonify({
        'code': 200,
        'value': "yes"
    })
    # os.remove('uploads/' + file.filename)

    return return_data


@app.route('/getSelect', methods=['get'])
def getSelect():
    with open('localData.json', 'r') as file:
        data = json.load(file)

    # 返回值
    return_data = jsonify({
        'code': 200,
        'data': data
    })

    # os.remove('uploads/' + file.filename)
    return return_data


@app.route('/updateSelect', methods=['POST'])
def updateSelect():
    # 获取 JSON 格式的请求数据
    data = request.get_json()
    testItem = data.get('testItem')
    concentration = data.get('concentration')
    testTime = data.get('testTime')

    dataJson = {
        "testItem": testItem,
        "concentration": concentration,
        "testTime": testTime
    }

    # 返回值
    return_data = jsonify({
        'code': 200,
        'value': "yes"
    })
    # 写入到 localData.json 文件
    with open('localData.json', 'w') as file:
        json.dump(dataJson, file, indent=4)
    return return_data


@app.route('/getTime', methods=['get'])
def getTime():
    current_time = datetime.now()
    milliseconds = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    # 返回值
    return_data = jsonify({
        'code': 200,
        'data': milliseconds
    })

    return return_data


# http服务器
if __name__ == '__main__':
    current_time = datetime.now()
    milliseconds = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    print(milliseconds)
    app.run(host="0.0.0.0", port=6868, threaded=True)
