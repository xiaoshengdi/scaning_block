import os


# Define the directory path where your files are located
directory = 'uploads/samsung/'


# Function to rename files
# Function to rename files
def rename_files(directory):
    for filename in os.listdir(directory):
        # 检查文件名是否包含两个或更多的下划线
        if filename.count('_') >= 2:
            parts = filename.split('_')
            # 获取第二个下划线后的部分并进行替换
            if parts[2] == '0':
                parts[2] = '1'
            elif parts[2] == '1':
                parts[2] = '2'
            elif parts[2] == '3':
                parts[2] = '2'
            elif parts[2] == '4':
                parts[2] = '3'
            new_filename = '_'.join(parts)
            old_filepath = os.path.join(directory, filename)
            new_filepath = os.path.join(directory, new_filename)
            os.rename(old_filepath, new_filepath)
            print(f'Renamed: {old_filepath} -> {new_filepath}')
        else:
            print(f'Skipped: {filename}')


rename_files(directory)
