from flask import Flask, request, jsonify
#import test_similarity as test
#import tc_rgb_test_similarity_v1 as test
#import tc_rgb_test_similarity as test
#import test_similarity as test
import polyblock_test as test

import os
import json

app = Flask(__name__)


@app.route('/upload_file', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    if file.filename == '':
        return jsonify({'error': 'No selected file'})
    print(file.filename)

    # 保存上传的文件至服务器
    file.save('uploads/' + file.filename)

    # 相似值
    try:
        value = test.similarity_localImage('uploads/' + file.filename)
        print(value)
    except Exception as e:
        value = -2000
        print(e)

    for item in value:
        item_dict = json.loads(item)
        filename = str(item_dict["name"]) + ".txt"
        # Write the "value" field to the file
        with open(filename, 'a') as f:
            f.write(str(item_dict['value'])+ "\n")  # Write the dictionary entry as a string to the file


    # 返回值
    return_data = jsonify({
        'code': 200,
        'value': value
    })
    # os.remove('uploads/' + file.filename)

    return return_data


# http服务器
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8887, threaded=True)
