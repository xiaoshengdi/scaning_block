import json
import os
import re
import matplotlib.pyplot as plt
import numpy as np
import cv2
import matplotlib.pyplot as plt


def rgb_to_grayscale(red, green, blue):
    grayscale = 0.2989 * red + 0.5870 * green + 0.1140 * blue
    return grayscale


def get_box_dimensions(box):
    x1, y1, x2, y2 = box  # Unpack the box coordinates
    width = x2 - x1  # Calculate width
    height = y2 - y1  # Calculate height
    return width, height


# for i in range(0, 56):
#     filename = str(i) + ".txt"
#     with open(filename, 'w') as f:
#         pass  # Write nothing to the file

