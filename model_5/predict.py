import fastdeploy as fd
import fastdeploy.vision as vision
import cv2
import os



# FastDeploy切换后端和硬件
option = fd.RuntimeOption()
option.use_cpu()
option.use_openvino_backend()
model = fd.vision.detection.PPYOLOE("model_5/model.pdmodel",
                                    "model_5/model.pdiparams",
                                    "model_5/infer_cfg.yml", runtime_option=option)


# 根据模型发现试剂，并返回置信度大于0.9的区域
def detection(img,name):
    """ 根据模型发现试剂
       :param im_data: (numpy.ndarray) 输入图片
       :return: (numpy.ndarray) 发现区域后，裁剪置信度大于0.9的区域图片
       """
    # 推理图像
    result = model.predict(img)
    vis_im = vision.vis_detection(img, result, score_threshold=0.5)
    cv2.namedWindow('vis_image8.jpg', cv2.WINDOW_NORMAL)  # 窗口大小自适应比例
    cv2.imshow("vis_image8.jpg", vis_im)
    file_prefix = os.path.splitext(name)[0]

    #cv2.imwrite(file_prefix+"_copy"+".jpg",vis_im)

    boxes = result.boxes
    scores = result.scores
    label_ids = result.label_ids
    detection_results = [
        DetectionResult(*box, score, label_id)
        for box, score, label_id in zip(boxes, scores, label_ids)
    ]

    highest_scores = {}

    # Iterate through all results
    for result in detection_results:
        label_id = result.label_id
        score = result.score

        # Check if this label_id has been seen before or if the current score is higher
        if label_id not in highest_scores or score > highest_scores[label_id]:
            highest_scores[label_id] = score

    # Filter the detection_results to keep only the highest-scored entry for each label_id
    filtered_results = [
        result
        for result in detection_results
        if result.score == highest_scores[result.label_id]
        #if result.score > 0.2 and result.score == highest_scores[result.label_id]
    ]
    return filtered_results;


class DetectionResult:
    def __init__(self, xmin, ymin, xmax, ymax, score, label_id):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.score = score
        self.label_id = label_id

        # Initialize filtered attributes
        self.filtered_xmin = xmin
        self.filtered_ymin = ymin
        self.filtered_xmax = xmax
        self.filtered_ymax = ymax
        self.filtered_score = score
        self.filtered_label_id = label_id
