import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Data points for the first set
gray1 = np.array([149.5206612, 162.4958678, 136.2518519, 162.2666667])
hsv1 = np.array([42.39130435, 44, 49.50857143, 40.13142857])

# Data points for the second set
gray2 = np.array([230, 228, 192, 222.4367816])
hsv2 = np.array([28.85217391, 46.22608696, 52.3258427, 21.96062992])


# Function to calculate y values given x and coefficients
def calculate_y(x, m, c):
    return m * x + c


def test1():
    # Calculate coefficients for the first set
    m1 = (hsv1[2] - hsv1[0]) / (gray1[2] - gray1[0])
    c1 = hsv1[0] - m1 * gray1[0]

    # Calculate coefficients for the second set
    m2 = (hsv2[2] - hsv2[0]) / (gray2[2] - gray2[0])
    c2 = hsv2[0] - m2 * gray2[0]

    # Calculate combined coefficients (average slope)
    m_combined = (m1 + m2) / 2

    # Adjust intercepts to make Y values similar
    # Choose c1 for simplicity (adjust as needed based on your analysis)
    c_combined = (gray1[0] * m1 + gray2[0] * m2) / (gray1[0] + gray2[0])

    # Generate x values for plotting
    x_values = np.linspace(130, 240, 100)  # Adjust range as per your data

    # Calculate y values for both equations
    y_values1 = calculate_y(x_values, m1, c1)
    y_values2 = calculate_y(x_values, m2, c2)
    y_values_combined = calculate_y(x_values, m_combined, c_combined)

    # Plotting
    plt.figure(figsize=(10, 6))

    # Plotting the first set of data points and adjusted equation
    plt.scatter(hsv1, gray1, color='blue', label='First Set')
    plt.plot(x_values, y_values1, color='blue', linestyle='--', label='Equation 1')

    # Plotting the second set of data points and adjusted equation
    plt.scatter(hsv2, gray2, color='red', label='Second Set')
    plt.plot(x_values, y_values2, color='red', linestyle='--', label='Equation 2')

    # Plotting the adjusted combined equation
    plt.plot(x_values, y_values_combined, color='green', label='Adjusted Combined Equation')

    # Add labels and title
    plt.xlabel('HSV')
    plt.ylabel('Gray')
    plt.title('Adjusted Linear Regression of HSV vs. Gray Values')
    plt.legend()

    # Show plot
    plt.grid(True)
    plt.show()

    # Print the adjusted equations
    print("Equation 1 (Adjusted): y = {:.6f}x + {:.6f}".format(m1, c1))
    print("Equation 2 (Adjusted): y = {:.6f}x + {:.6f}".format(m2, c2))
    print("Adjusted Combined Equation: y = {:.6f}x + {:.6f}".format(m_combined, c_combined))


# Original datasets
data1 = {
    "phoneName": ["iPhone", "Redmi", "Samsung", "Vivo"],
    "Gray": [149.5206612, 162.4958678, 136.2518519, 162.2666667],
    "HSV": [42.39130435, 44, 49.50857143, 40.13142857]
}

data2 = {
    "phoneName": ["iPhone", "Redmi", "Samsung", "Vivo"],
    "Gray": [230, 228, 192, 222.4367816],
    "HSV": [28.85217391, 46.22608696, 52.3258427, 21.96062992]
}

# Convert to DataFrame
df1 = pd.DataFrame(data1)
df2 = pd.DataFrame(data2)

# Normalize both datasets (min-max normalization)
df1_norm = (df1[["Gray", "HSV"]] - df1[["Gray", "HSV"]].min()) / (
            df1[["Gray", "HSV"]].max() - df1[["Gray", "HSV"]].min())
df2_norm = (df2[["Gray", "HSV"]] - df2[["Gray", "HSV"]].min()) / (
            df2[["Gray", "HSV"]].max() - df2[["Gray", "HSV"]].min())

# Calculate adjustment needed to align df1 with df2
adjustment = df2_norm - df1_norm

# Apply adjustment to original df1
adjusted_df1 = df1[["Gray", "HSV"]] + adjustment * (df1[["Gray", "HSV"]].max() - df1[["Gray", "HSV"]].min())

# Combine adjusted values back with phone names
adjusted_df1["phoneName"] = df1["phoneName"]

# Calculate CV for both datasets
cv_original = df1[["Gray", "HSV"]].std() / df1[["Gray", "HSV"]].mean()
cv_adjusted = adjusted_df1[["Gray", "HSV"]].std() / adjusted_df1[["Gray", "HSV"]].mean()

print("Original Data CV:\n", cv_original)
print("Adjusted Data CV:\n", cv_adjusted)
print("Adjusted Data:\n", adjusted_df1)
