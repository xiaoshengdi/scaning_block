import fastdeploy as fd
import fastdeploy.vision as vision
import cv2
import os
import random
import numpy as np
import math

option = fd.RuntimeOption()
option.use_cpu()
option.use_openvino_backend()
model = fd.vision.detection.PPYOLOE("fsh_model/model.pdmodel",
                                    "fsh_model/model.pdiparams",
                                    "fsh_model/infer_cfg.yml", runtime_option=option)

class DetectionResult:
    def __init__(self, xmin, ymin, xmax, ymax, score, label_id):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.score = score
        self.label_id = label_id

        # Initialize filtered attributes
        self.filtered_xmin = xmin
        self.filtered_ymin = ymin
        self.filtered_xmax = xmax
        self.filtered_ymax = ymax
        self.filtered_score = score
        self.filtered_label_id = label_id


def run_inference_for_single_image(model, image):
    result = model.predict(image)


    vis_im = vision.vis_detection(image, result, score_threshold=0.3)
    # cv2.namedWindow('vis_image8.jpg', cv2.WINDOW_FREERATIO)  # 窗口大小自适应比例
    cv2.imshow("vis_image8.jpg", vis_im)

    boxes = result.boxes
    scores = result.scores
    label_ids = result.label_ids
    detection_results = [
        DetectionResult(*box, score, label_id)
        for box, score, label_id in zip(boxes, scores, label_ids)
    ]

    highest_scores = {}

    # Iterate through all results
    for result in detection_results:
        label_id = result.label_id
        score = result.score

        # Check if this label_id has been seen before or if the current score is higher
        if label_id not in highest_scores or score > highest_scores[label_id]:
            highest_scores[label_id] = score

    # Filter the detection_results to keep only the highest-scored entry for each label_id
    filtered_results = [
        result
        for result in detection_results
        if result.score == highest_scores[result.label_id]
    ]
    for result in filtered_results:
        print(
            f"Label ID: {result.label_id}, "
            f"Box: ({result.xmin}, {result.ymin}, {result.xmax}, {result.ymax}), "
            f"Score: {result.score}"
        )

    print(filtered_results)

    return filtered_results;





cap = cv2.VideoCapture(0)
while True:
    ret, frame = cap.read()
    output_dict = run_inference_for_single_image(model, frame)






    cv2.imshow('Object Detection', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()