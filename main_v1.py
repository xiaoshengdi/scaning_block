from flask import Flask, request, jsonify
import test_similarity as test
import os
import json

app = Flask(__name__)


@app.route('/upload_file', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'})

    file = request.files['file']

    if file.filename == '':
        return jsonify({'error': 'No selected file'})
    print(file.filename)

    path = 'uploads/'

    # 保存上传的文件至服务器
    file.save(path + file.filename)

    # 相似值
    try:
        similarity = test.similarity_localImage_1(path + file.filename)
        print(similarity)
    except Exception as e:
        similarity = -2000
        print(e)

    for item in similarity:
        item_dict = json.loads(item)
        filename = "data/"+str(item_dict["name"]) + ".txt"
        # Write the "value" field to the file
        with open(filename, 'a') as f:
            f.write(str(item_dict['value']) + "\n")  # Write the dictionary entry as a string to the file

    # 返回值
    return_data = jsonify({
        'code': 200,
        'value': similarity,
    })
    os.remove(path + file.filename)
    return return_data
    # return jsonify({'message': 'File uploaded successfully', 'filename': file.filename})


# http服务器
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8887, threaded=True)
